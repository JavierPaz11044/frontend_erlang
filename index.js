var socket;
const get_all_users = "http://localhost:8090/api/server_handler:get_all_numbers"
const get_numbers = "http://localhost:8090/api/server_handler:get_numbers/"
const create_number = "http://localhost:8090/api/server_handler:create_number/"
const call_number = "http://localhost:8090/api/server_handler:call/"
const get_call = "http://localhost:8090/api/server_handler:get_call/"
const receive_call = "http://localhost:8090/api/server_handler:received_call/"
const close_call = "http://localhost:8090/api/server_handler:close_all/"
const my_number =  document.getElementById('my_number')
const numbers =  document.getElementById('numbers')
var time_out_waiting, timer_out_receive, close_received
var cont = 0
var is_called
var my_cellphone 
const paintTable = array => {
    numbers.innerHTML = ''
    const fragment = document.createDocumentFragment()
    const table = document.createElement("table")
    const tr_tittle = document.createElement('tr')
    const th_tittle = document.createElement('th')
    th_tittle.innerHTML = 'Numbers'
    tr_tittle.appendChild(th_tittle)
    table.appendChild(tr_tittle)
    array.forEach(num => {
    const tr_number = document.createElement('tr')
    const th_number = document.createElement('th')
    th_number.innerHTML = num
    tr_number.appendChild(th_number)
    table.appendChild(tr_number)
    })
    fragment.appendChild(table)
    numbers.appendChild(fragment)
}
const getNumbers = async () => {
    const socket = new XMLHttpRequest();
	socket.onreadystatechange =  function() {
        var array_numbers_all =  socket.responseText.split("")
        var array_number_group = []
        for (let  cont = 0 ; cont < socket.responseText.length-4; cont+=4){
            array_number_group.push(socket.responseText.slice(cont, cont+4))
        }
        paintTable(array_number_group)
	}

   socket.open('GET', get_all_users, true);
   socket.setRequestHeader('X-PINGOTHER', 'pingpong')
   socket.setRequestHeader('Accept', 'application/json')
   socket.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:8090')
   socket.setRequestHeader('GET', 'POST', 'OPTIONS')
   await socket.send() 
}

const start = async () => {
    random = Math.floor((Math.random()  * 1000) + 8000); 
    my_number.innerHTML = `<h3> My Number is ${random}`
    my_cellphone = random
    socket =  new XMLHttpRequest();
	socket.onreadystatechange =  function() {
        console.log(socket.responseText)
		if (this.readyState == 4 && this.status == 200) {
			console.log(socket.responseText)
		 }
	}
    
   socket.open('GET', `${create_number}${random}true`, true);
   socket.setRequestHeader('X-PINGOTHER', 'pingpong')
   socket.setRequestHeader('Accept', 'application/json')
   socket.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:8090')
   socket.setRequestHeader('GET', 'POST', 'OPTIONS')
   await socket.send()
getNumbers();
}

const call  = async (e) =>{

    const socket = new XMLHttpRequest();
    
    const value =  document.getElementById('number').value
    is_called = value
    console.log(value,`${call_number}${value}${my_cellphone}`)
   socket.open('GET', `${call_number}${value}${my_cellphone}`, true);
   socket.setRequestHeader('X-PINGOTHER', 'pingpong')
   socket.setRequestHeader('Accept', 'application/json')
   socket.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:8090')
   socket.setRequestHeader('GET', 'POST', 'OPTIONS')
   await socket.send() 
   waitingResponse();
}

const waitingResponse = async () => {
    console.log("waiting")
    socket.onreadystatechange =  function() {
        console.log(socket.responseText)
        const state = socket.responseText.slice(4,8)
        console.log(state.trim() === 'fals')
		if (state.trim() === 'fals'){
            console.log("succes", state)
            clearTimeout(timer_out_receive)
            clearTimeout(time_out_waiting)
        }else {
            console.log("error", state)
        }
	}
    const value =  document.getElementById('number').value
   socket.open('GET', `${get_numbers}${value}`, true);
   socket.setRequestHeader('X-PINGOTHER', 'pingpong')
   socket.setRequestHeader('Accept', 'application/json')
   socket.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:8090')
   socket.setRequestHeader('GET', 'POST', 'OPTIONS')
   await socket.send()
   time_out_waiting = setTimeout(waitingResponse, 5000)
}

const receive = async () =>{
    console.log("waiting")
    const socket = new XMLHttpRequest();
    socket.onreadystatechange =  async function() {
        console.log(socket.responseText)
        const state = socket.responseText.slice(4,8)
		if (state.trim().length === 4){
            console.log("te estan llamando", state)
            cont = 0
            contestar();
            const socket = new XMLHttpRequest();
            const value =  document.getElementById('number').value
            console.log(value,`${call_number}${value}${my_cellphone}`)
            is_called =  my_cellphone
           socket.open('GET', `${receive_call}${my_cellphone}false`, true);
           socket.setRequestHeader('X-PINGOTHER', 'pingpong')
           socket.setRequestHeader('Accept', 'application/json')
           socket.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:8090')
           socket.setRequestHeader('GET', 'POST', 'OPTIONS')
           await socket.send() 
            clearTimeout(timer_out_receive)
        }else {
            console.log("error", state)
        }
	}
   socket.open('GET', `${get_call}${my_cellphone}`, true);
   socket.setRequestHeader('X-PINGOTHER', 'pingpong')
   socket.setRequestHeader('Accept', 'application/json')
   socket.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:8090')
   socket.setRequestHeader('GET', 'POST', 'OPTIONS')
   await socket.send()
   timer_out_receive = setTimeout(receive, 5000)
}

const in_called =(number) => {

}

const contestar = () => {
    const button = document.getElementById("contestar")
    button.style.background = "black"
    document.getElementById('received').innerHTML = ''
    document.getElementById('received').innerHTML = cont
    if (cont >= 5){
        finish();
        button.style.background = "blue"
        clearTimeout(close_received)
    }else{
        cont +=1
    }
    close_received = setTimeout(contestar, 4000 )
}

const finish = async () => {
    console.log("waiting")
    const socket = new XMLHttpRequest();
    socket.onreadystatechange =  function() {
        console.log(socket.responseText)
	}
   socket.open('GET', `${close_call}${is_called}`, true);
   socket.setRequestHeader('X-PINGOTHER', 'pingpong')
   socket.setRequestHeader('Accept', 'application/json')
   socket.setRequestHeader('Access-Control-Allow-Origin', 'http://localhost:8090')
   socket.setRequestHeader('GET', 'POST', 'OPTIONS')
   await socket.send()
   receive();
}
window.onload = start()
receive();
//document.getElementById('call').addEventListener('click', call())